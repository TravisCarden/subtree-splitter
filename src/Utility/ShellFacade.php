<?php

namespace DrupalCoreSplit\Utility;

use RuntimeException;

class ShellFacade {

  public function exec(string $command): array {
    print "> {$command}\n";
    exec($command, $output, $return_var);
    if ($return_var) {
      throw new RuntimeException('Shell command failed.');
    }
    return $output;
  }

  public function passthru(string $command, bool $fail = TRUE) {
    print "> {$command}\n";
    passthru($command, $return_var);
    if ($return_var && $fail) {
      throw new RuntimeException('Shell command failed.');
    }
  }

}
