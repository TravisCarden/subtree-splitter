<?php

namespace DrupalCoreSplit\PostProcessor;

use DrupalCoreSplit\Utility\ShellFacade;

class ChangeOrgProcessor implements PostProcessorInterface {

  protected $org;
  private $shell;

  public function __construct($org) {
    $this->org = $org;
    $this->shell = new ShellFacade();
  }

  /**
   * {@inheritdoc}
   */
  public function process($directory, $ref, $reftype, $name) {
    $composer_json = json_decode(file_get_contents("$directory/composer.json"), TRUE);

    $org_and_project = $composer_json['name'];
    $project = basename($org_and_project);

    $composer_json['name'] = "{$this->org}/$project";

    file_put_contents("$directory/composer.json", json_encode($composer_json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $this->shell->passthru("git -C {$directory} add composer.json");

    $original_committer_date = $this->shell->exec("git -C {$directory} log -1 --pretty=%cd");
    $this->shell->passthru("GIT_COMMITTER_DATE=\"{$original_committer_date[0]}\" git -C {$directory} commit --amend -C HEAD");
  }

}
