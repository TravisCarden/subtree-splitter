<?php

namespace DrupalCoreSplit\PostProcessor;

interface PostProcessorInterface {

  /**
   * Post-processes the item.
   *
   * @param string $directory
   *   Directory where subtree-split project files have been checked out.
   * @param string $ref
   *   The branch name or tag being processed.
   * @param string $reftype
   *   Whether a branch or tag is being processed.
   * @param string $name
   *   The name of the subtree-split project being processed.
   *
   * @return bool
   *   Returns TRUE if any files were modified.
   */
  public function process($directory, $ref, $reftype, $name);

}
