<?php

namespace DrupalCoreSplit;

use DrupalCoreSplit\Command\ConfigCommand;
use DrupalCoreSplit\Command\PushPackagistCommand;
use DrupalCoreSplit\Command\PurgeCommand;
use DrupalCoreSplit\Command\PushCommand;
use DrupalCoreSplit\Command\SplitCommand;
use DrupalCoreSplit\Command\CreateLockCommand;
use Webmozart\Console\Api\Args\Format\Argument;
use Webmozart\Console\Api\Args\Format\Option;
use Webmozart\Console\Config\DefaultApplicationConfig;

class AppConfig extends DefaultApplicationConfig {

  protected function configure() {
    parent::configure();

    $this->beginCommand('config')
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->setHandler(new ConfigCommand());

    $this->beginCommand('push')
      ->addArgument('ref', Argument::REQUIRED)
      ->addArgument('sha1', Argument::REQUIRED)
      ->addArgument('reftype', Argument::REQUIRED)
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->addOption('no-packagist', NULL, Option::NO_VALUE)
      ->addOption('push-all', NULL, Option::NO_VALUE)
      ->setHandler(new PushCommand());

    $this->beginCommand('push-packagist')
      ->addArgument('project-name', Argument::REQUIRED)
      ->addArgument('ref', Argument::OPTIONAL)
      ->addArgument('sha1', Argument::OPTIONAL)
      ->addArgument('reftype', Argument::OPTIONAL)
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->addOption('no-packagist', NULL, Option::NO_VALUE)
      ->addOption('push-all', NULL, Option::NO_VALUE)
      ->setHandler(new PushPackagistCommand());

    $this->beginCommand('split')
      ->addArgument('ref', Argument::REQUIRED)
      ->addArgument('sha1', Argument::REQUIRED)
      ->addArgument('reftype', Argument::REQUIRED)
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->addOption('split-all', NULL, Option::NO_VALUE)
      ->setHandler(new SplitCommand());

    $this->beginCommand('purge')
      ->addArgument('ref', Argument::REQUIRED)
      ->addArgument('sha1', Argument::REQUIRED)
      ->addArgument('reftype', Argument::REQUIRED)
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->setHandler(new PurgeCommand());

    $this->beginCommand('create-lock')
      ->addArgument('ref', Argument::REQUIRED)
      ->addArgument('sha1', Argument::REQUIRED)
      ->addArgument('reftype', Argument::REQUIRED)
      ->addOption('config', NULL, Option::REQUIRED_VALUE, 'A path to a configuration file. See config/config.yml.dist for the schema.')
      ->setHandler(new CreateLockCommand());
  }

}
