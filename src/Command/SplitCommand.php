<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\PostProcessor\VaultProcessor;
use RuntimeException;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;
use DrupalCoreSplit\PostProcessor\ChangeOrgProcessor;

class SplitCommand extends CommandBase {

  private $processors = [];

  public function handle(Args $args, IO $io) {
    $this->handleCommandArguments($args, $io);
    try {
      $this->setUpSourceRepository();
      $this->addPostSplitProcessors();
      $this->generateSubtrees();
    }
    catch (RuntimeException $e) {
      $io->errorLine($e->getMessage());
      return 1;
    }
  }

  private function addPostSplitProcessors() {
    if (!empty($this->config->getRemapOrg())) {
      $this->processors[] = new ChangeOrgProcessor($this->config->getGithubOrgName());
    }
    $this->processors[] = new VaultProcessor($this->getProjectVaultDir());
  }

  private function generateSubtrees() {
    if ($this->reftype == 'branch') {
      if ($this->args->getOption('split-all')) {
        $all_subtrees = $this->subtrees->getAll($this->getProjectSourceDir());
      }
      else {
        $all_subtrees = $this->getChangedSubtrees($this->sha1);
      }

      foreach ($all_subtrees as $subtree_name => $subtree_data) {

        $this->printHeading("Splitting {$subtree_name}");

        $subtree_dir = $this->getGeneratedSubtreeDir($subtree_name);
        $this->shell->passthru("rm -rf {$subtree_dir}");
        $this->shell->passthru("git -c advice.detachedHead=false clone -q {$this->getProjectSourceDir()} {$subtree_dir}");

        $this->io->writeLine("Splitting branch {$this->ref}");
        // Update branch.
        $this->splitBranch($subtree_data['path'], $subtree_name);

      }
    }
    if ($this->reftype == 'tag') {
      $core_branch = static::getCoreBranch($this->ref);
      $new_sha1 = $this->shell->exec("/bin/bash -c \"diff --old-line-format='' --new-line-format='' <(git -C {$this->getProjectSourceDir()} rev-list --first-parent {$this->ref}) <(git -C {$this->getProjectSourceDir()} rev-list --first-parent remotes/origin/{$core_branch}) |head -1\"");

      // For tags we want to go one earlier than what we know, just in case
      // we only have the one commit. Hence the ^ in the arg below.
      $changed_subtrees = $this->getChangedSubtrees("{$new_sha1[0]}^");
      $this->io->writeLine("Splitting tag {$this->ref}");
      foreach ($changed_subtrees as $c_subtree_name => $c_subtree_data) {
        try {
          // If the tag is *not* there, the command fails, and throws an exception
          // Which tells us we need to create the tag.
          $this->shell->passthru("git -C {$this->getProjectVaultDir()}/{$c_subtree_name} rev-parse {$this->ref} >/dev/null 2>&1");
        }
        catch (RuntimeException $e) {
          $c_subtree_dir = $this->getGeneratedSubtreeDir($c_subtree_name);
          $this->shell->passthru("rm -rf {$c_subtree_dir}");
          $this->shell->passthru("git -c advice.detachedHead=false clone -q {$this->getProjectSourceDir()} {$c_subtree_dir}");
          $this->splitTag($c_subtree_data['path'], $c_subtree_name);
        }
      }

      $all_subtrees = $this->subtrees->getAll($this->getProjectSourceDir());
      // If this is a tag, then we should tag the rest of the subtrees that we
      // Did not split yet.
      // We should attach the tag to the first commit that is equal to or
      // earlier than the date of the tag in the upstream repo.
      foreach ($all_subtrees as $subtree_name => $subtree_data) {
        if (!empty($changed_subtrees[$subtree_name])) {
          // We already split these subtrees, so no need to push/tag again.
          continue;
        }
        try {
          // If the tag is *not* there, the command fails, and throws an exception
          // Which tells us we need to create the tag.
          $this->shell->passthru("git -C {$this->getProjectVaultDir()}/${subtree_name} rev-parse {$this->ref} >/dev/null 2>&1");
        }
        catch (RuntimeException $e) {

          // Get the branch of destination from the tag.
          $core_branch = static::getCoreBranch($this->ref);
          // Get the date of the tag from the parent repo.
          // get the commit hash of the commit that we need to tag from the repo
          // then push the tag. to the vault.
          $commit_hash = $this->shell->exec("git -C {$this->getProjectVaultDir()}/${subtree_name} log -1 --format=%H --date=unix --before=`git -C {$this->getProjectSourceDir()} tag -l --format \"%(creatordate:unix)\" {$this->ref}` ${core_branch}");

          $this->shell->passthru("git -C {$this->getProjectVaultDir()}/${subtree_name} tag {$this->ref} {$commit_hash[0]}");
        }
      }
    }
  }

  protected function splitBranch($prefix = 'core', $name = 'core') {
    $subtree_dir = $this->getGeneratedSubtreeDir($name);
    $this->shell->passthru("cd {$subtree_dir} && git -c advice.detachedHead=false checkout --force {$this->ref} && git reset --hard origin/{$this->ref}");
    $this->shell->exec("splitsh-lite --prefix={$prefix}/ --origin=origin/{$this->ref} --path={$subtree_dir} --target=HEAD");

    // splitsh-lite finishes with the commit for the split tree being at the
    // HEAD of the current branch; however, the files in the local working set
    // are not changed, and still contain the files from the pre-split repo.
    // Resetting makes the local files contain the most recent split tree.
    $this->shell->passthru("git -C {$subtree_dir} reset --hard");

    $this->postProcess($name);
  }

  protected function splitTag($prefix = 'core', $name = 'core') {
    $subtree_dir = $this->getGeneratedSubtreeDir($name);
    $this->shell->passthru("cd {$subtree_dir} && git fetch --tags");

    $output = $this->shell->exec("splitsh-lite --prefix={$prefix}/ --origin=tags/{$this->ref} --path={$subtree_dir} --target=tags/{$this->ref}");

    // splitsh outputs the sha of the commit for the split repo
    $split_ref = $output[0];
    $this->shell->passthru("git -C {$subtree_dir} -c advice.detachedHead=false checkout {$split_ref}");

    $changed = $this->postProcess($name);
    if ($changed) {
      $this->shell->passthru("git -C {$subtree_dir} tag -f {$this->ref}");
    }
  }

  protected function postProcess($name): bool {
    $changed = FALSE;

    foreach ($this->processors as $processor) {
      $changed |= $processor->process($this->getGeneratedSubtreeDir($name), $this->ref, $this->reftype, $name);
    }

    return $changed;
  }

}
