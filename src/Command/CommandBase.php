<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\Utility\ConfigHandler;
use DrupalCoreSplit\Utility\ShellFacade;
use DrupalCoreSplit\Utility\SubtreeHandler;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;

abstract class CommandBase {

  /** @var Args */
  protected $args;
  protected $config;
  /** @var IO */
  protected $io;
  protected $ref;
  protected $reftype;
  protected $sha1;
  protected $shell;
  protected $subtrees;
  protected $templates = ['recommended-project', 'legacy-project'];

  public function __construct() {
    $this->config = new ConfigHandler();
    $this->shell = new ShellFacade();
    $this->subtrees = new SubtreeHandler();
  }

  protected function getParentWorkingDir(): string {
    return "{$this->config->getWorkingDirectory()}/{$this->config->getProjectToSplit()}/{$this->ref}/{$this->sha1}";
  }

  protected function getGeneratedSubtreeDir(string $name): string {
    return "{$this->getParentWorkingDir()}/generated-subtrees/{$name}";
  }

  protected function getSubtreeCheckoutDir(string $name): string {
    return "{$this->getParentWorkingDir()}/subtree-workdirs/{$name}";
  }

  protected function getProjectSourceDir(): string {
    return "{$this->getParentWorkingDir()}/source-project";
  }

  protected function getProjectVaultDir(): string {
    return "{$this->config->getWorkingDirectory()}/subtree-vaults/{$this->config->getProjectToSplit()}";
  }

  protected function getProjectVaultSubtreeDir(string $name): string {
    return "{$this->getProjectVaultDir()}/{$name}";
  }

  protected function handleCommandArguments(Args $args, IO $io) {
    $filename = $args->getOption('config');
    if ($filename) {
      $this->config->setConfigOverrideFile($filename);
    }
    $this->args = $args;
    $this->io = $io;
    $this->ref = $args->getArgument('ref');
    $this->sha1 = $args->getArgument('sha1');
    $this->reftype = $args->getArgument('reftype');
  }

  protected function printHeading($message) {
    $this->io->writeLine('');
    $this->io->writeLine('=====================================================');
    $this->io->writeLine($message);
    $this->io->writeLine('-----------------------------------------------------');
  }

  protected function setUpSourceRepository() {
    if (!file_exists($this->getProjectSourceDir())) {
      $this->shell->passthru("git -c advice.detachedHead=false clone -q {$this->config->getProjectUpstream()} {$this->getProjectSourceDir()}");
    }
    $this->shell->passthru("cd {$this->getProjectSourceDir()} && git remote set-url origin {$this->config->getProjectUpstream()} && git fetch origin && git fetch -t origin && git -c advice.detachedHead=false checkout {$this->ref}");
  }

  /**
   * @param $delta_sha string
   *   This is the sha1 of the commit that we wish to compare our ref against.
   *
   * @return array
   */
  protected function getChangedSubtrees($delta_sha) {
    $subtrees = $this->subtrees->getAll($this->getProjectSourceDir());
    $files_changed_since_last_commit = $this->shell->exec("git -C {$this->getProjectSourceDir()} log --name-only --oneline {$delta_sha}.. --format= {$this->ref}");
    $allfiles = json_encode($files_changed_since_last_commit);
    foreach ($subtrees as $subtree_name => $subtree_data) {

      // If the subtree path doesnt show up in the list of all paths, we'll
      // skip processing it.
      $encoded_subtree_path = str_replace('"', '', json_encode($subtree_data['path']));
      if (!strpos($allfiles, $encoded_subtree_path)) {
        unset($subtrees[$subtree_name]);
      }

    }
    return $subtrees;
  }

  public function getChangedVaultRepos($delta_sha): array {
    $vault_repos = $this->subtrees->getVaultRepos($this->getProjectVaultDir());
    $changed_subtrees = $this->getChangedSubtrees($delta_sha);
    $changed_repos = [];
    foreach ($vault_repos as $repo) {
      if (isset($changed_subtrees[$repo])) {
        $changed_repos[] = $repo;
      }
    }
    if ($this->reftype == 'branch') {
      foreach ($this->templates as $template) {
        if (!in_array($template, $changed_repos)) {
          $changed_repos[] = $template;
        }
      }
    }

    return $changed_repos;
  }

  /**
   * Get the "core branch” of a version.
   *
   * @param string $version
   *   The version number to process.
   *
   * @return string
   *   The version number with everything after the last '.' stripped.
   */
  public static function getCoreBranch($version) {
    return preg_replace('#\.[^.]*$#', '.', $version) . "x";
  }

}
