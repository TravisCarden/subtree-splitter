<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\Utility\GitHubApi;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;
use GuzzleHttp\Client as GuzzleClient;
use Packagist\Api\Client as PackagistClient;

class PushCommand extends CommandBase {

  public function handle(Args $args, IO $io) {
    $this->handleCommandArguments($args, $io);
    try {
      $this->setUpSourceRepository();
      $this->pushSubtrees();
    }
    catch (RuntimeException $e) {
      $io->errorLine($e->getMessage());
      return 1;
    }
  }

  private function pushSubtrees() {
    if ($this->reftype === 'branch' && !$this->args->getOption('push-all')) {
      $vault_repos = $this->getChangedVaultRepos($this->sha1);
    }
    else {
      $vault_repos = $this->subtrees->getVaultRepos($this->getProjectVaultDir());
    }

    $github = new GitHubApi($this->config);
    $github_repos = $github->getRepos();
    foreach ($vault_repos as $repo_name) {
      try {
        $this->shell->exec("git -C {$this->getProjectVaultSubtreeDir($repo_name)} rev-parse {$this->ref} >/dev/null 2>&1");
      }
      catch (Exception $e) {
        $this->io->errorLine($e->getMessage());
        continue;
      }
      $this->printHeading("Pushing {$repo_name}");

      // If there's no GitHub repository for the subtree, create one.
      if (!in_array($repo_name, $github_repos, TRUE)) {
        $composer_json = $this->shell->exec("git -C {$this->getProjectVaultSubtreeDir($repo_name)} show {$this->ref}:composer.json");
        $file_contents = implode('', $composer_json);
        $json = json_decode($file_contents);
        $this->io->writeLine("Creating {$repo_name} on GitHub");
        $github->createRepo($repo_name, $json->description);
      }

      if ($this->reftype == 'branch') {
        $this->io->writeLine("Update branch {$this->ref}");
        $this->pushBranch($repo_name);
      }
      else {
        $this->io->writeLine("Update tag {$this->ref}");
        $this->pushTag($repo_name);
      }

      if ($this->args->getOption('no-packagist')) {
        continue;
      }
      else {
        $url = $this->config->getGithubRepoUrlGit($repo_name);
        $this->updatePackagist($repo_name, $url);
      }

    }
  }

  protected function updatePackagist($subtree_name, $url) {
    $packagist_client = new PackagistClient();

    // Check if package exists:
    $package_name = "drupal/{$subtree_name}";
    $body = json_encode([
      'repository' => [
        'url' => $url,
      ],
    ]);
    try {
      $packagist_client->get($package_name);
      $endpoint = 'update-package';
    }
    catch (Exception $e) {
      // Package doesnt exist at packagist: needs to be created.
      $endpoint = 'create-package';
    }

    $packagist_api_client = new GuzzleClient(['base_uri' => 'https://packagist.org/api/']);

    try {
      $packagist_api_client->request('POST', $endpoint, [
        'body' => $body,
        'headers' => ['Content-Type' => 'application/json'],
        'query' => [
          'username' => $this->config->getGithubUsername(),
          'apiToken' => $this->config->getPackagistApiToken(),
        ],
      ]);
    }
    catch (GuzzleException $e) {
      throw new RuntimeException(sprintf('Failed to push to Packagist: %s', $e->getMessage()));
    }

  }

  protected function pushBranch($name = 'core') {
    $this->shell->passthru("git -C {$this->getProjectVaultSubtreeDir($name)} push --force {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}");
  }

  protected function pushTag($name = 'core') {
    try {
      // If the tag is *not* there, the command fails, and throws an exception
      // Which tells us we need to create the tag.
      $this->shell->passthru("git ls-remote --exit-code --tags {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}");
    }
    catch (RuntimeException $e) {
      $this->shell->passthru("git -C {$this->getProjectVaultSubtreeDir($name)} push {$this->config->getGithubRepoUrlHttp($name)} {$this->ref}", FALSE);
    }
  }

}
