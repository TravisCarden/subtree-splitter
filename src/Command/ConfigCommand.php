<?php

namespace DrupalCoreSplit\Command;

use DrupalCoreSplit\Utility\ConfigHandler;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;

class ConfigCommand {

  public function handle(Args $args, IO $io) {
    $config = new ConfigHandler();
    $filename = $args->getOption('config');
    if ($filename) {
      $config->setConfigOverrideFile($filename);
    }
    // Printing the output with var_export() may seem unintuitive, but it is
    // intentional: var_dump() isn't very readable, and print_r() doesn't show
    // data types or print boolean values.
    print var_export($config->getConfig());
  }

}
