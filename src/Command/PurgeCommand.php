<?php

namespace DrupalCoreSplit\Command;

use RuntimeException;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;

class PurgeCommand extends CommandBase {

  public function handle(Args $args, IO $io) {
    $this->handleCommandArguments($args, $io);
    try {
      $this->cleanupWorkDir();
    }
    catch (RuntimeException $e) {
      $io->errorLine($e->getMessage());
      return 1;
    }
  }

  private function cleanupWorkDir() {
    if (file_exists($this->getParentWorkingDir())) {
      $this->printHeading("Cleaning up {$this->getParentWorkingDir()}");
      $this->shell->passthru("rm -rf {$this->getParentWorkingDir()}");
    }
  }

}
