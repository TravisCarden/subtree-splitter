<?php

namespace DrupalCoreSplit\Command;

use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;

class PushPackagistCommand extends PushCommand {

  public function handle(Args $args, IO $io) {
    $this->handleCommandArguments($args, $io);
    try {
      $project_name = $this->args->getArgument('project-name');
      $url = "https://git.drupalcode.org/project/{$project_name}.git";
      $this->updatePackagist($project_name, $url);
    }
    catch (RuntimeException $e) {
      $io->errorLine($e->getMessage());
      return 1;
    }
  }

}
