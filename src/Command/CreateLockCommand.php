<?php

namespace DrupalCoreSplit\Command;

use RuntimeException;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\IO\IO;

class CreateLockCommand extends CommandBase {

  public function handle(Args $args, IO $io) {
    $this->handleCommandArguments($args, $io);
    try {
      $this->setUpSourceRepository();
      $this->createLockFiles();
    }
    catch (RuntimeException $e) {
      $io->errorLine($e->getMessage());
      return 1;
    }
  }

  private function createLockFiles() {
    foreach ($this->templates as $template) {
      $template_worktree = $this->getSubtreeCheckoutDir($template);
      // Clone the subtree so we have a working directory.
      if (!file_exists($template_worktree)) {
        $vault_subtree = $this->getProjectVaultSubtreeDir($template);
        try {
          $this->shell->passthru("git -C '{$vault_subtree}' show-ref '{$this->ref}'");
        }
        catch (RuntimeException $e) {
          $this->io->writeLine("Skipping {$template}, {$this->ref} does not exist in {$vault_subtree}");
          continue;
        }
        $this->shell->passthru("git -c advice.detachedHead=false clone -q --branch {$this->ref} {$vault_subtree} {$template_worktree}");
        $this->shell->passthru("git -C {$template_worktree} remote add vault {$this->getProjectVaultSubtreeDir($template)}");
      }
      if (file_exists("{$template_worktree}/composer.lock")) {
        $this->shell->passthru("rm {$template_worktree}/composer.lock");
      }
      $this->process($template_worktree, $this->ref, $this->reftype, $template);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function process($directory, $ref, $reftype, $name) {
    $php_path = $this->config->getPhpPath();
    $composer_command = "{$php_path} /usr/local/bin/composer --working-dir={$directory}";
    $stability = $this->stabilityFromRef($ref);

    $this->configureProjectComposerJson($directory, $stability);

    $versionConstraint = $this->versionConstraintFromRef($ref);

    // Find the list of `drupal/*` projects in the `require` section. We do not
    // want to have to change this tool whenever another Drupal dependency
    // is added.
    $dep_list = $this->getProjectsToConstrain($directory);

    // Tack the version constraint on the end of each dependency to update.
    $versioned_dep_list = array_map(
      static function ($dep) use ($versionConstraint) {
        return "$dep:$versionConstraint";
      },
      $dep_list
    );
    // This feeds the composer update command with the *direct* root dependencies
    // Because core-recommended itself requires drupal/core, we do not list it here
    $dependencies_to_update = implode(' ', $versioned_dep_list);

    $dep_names = array_map(
      static function ($package_name) {
        return str_replace('drupal/', '', $package_name);
      },
      $dep_list
    );
    // add core to the list of subtrees that we will check out from the vaults.
    $dep_names[] = 'core';

    // We'll need these for dev branches.
    if ($stability == 'dev') {
      $dep_names[] = "core-dev";
      $dep_names[] = "core-dev-pinned";
    }

    // Add path repositories to our local splits
    foreach ($dep_names as $package_name) {
      $dependency_worktree = $this->getSubtreeCheckoutDir($package_name);
      if (!file_exists($dependency_worktree)) {
        $this->shell->passthru("git -c advice.detachedHead=false clone -q --branch {$this->ref} {$this->getProjectVaultSubtreeDir($package_name)} {$dependency_worktree}");
      }
      if ($reftype === 'tag') {
        // Composer path repositories infer their “version” from the Git
        // environment. If there are multiple tags pointing at HEAD, it picks
        // one, probably the wrong one. Delete all the others.
        // https://github.com/composer/composer/issues/8890 could improve this.
        foreach ($this->shell->exec("git -C {$dependency_worktree} tag --points-at HEAD") as $head_tag) {
          if ($head_tag !== $this->ref) {
            $this->shell->passthru("git -C {$dependency_worktree} tag -d {$head_tag}");
          }
        }
      }
      $this->shell->passthru("{$composer_command} config repositories.{$package_name} '{\"type\": \"path\", \"url\": \"{$dependency_worktree}\"}'");
    }

    // Set the platform
    $this->shell->passthru("{$composer_command} config platform.php {$this->findMinimumPhp()}");

    // Create the lock file
    // Require the dependencies.
    $this->shell->passthru("{$composer_command} -n --update-no-dev require {$dependencies_to_update}");

    // For the dev versions of the templates, we have to do some magic with both core-dev-pinned and core-dev to ensure we get the exact correct versions in our lockfile.
    if ($stability == 'dev') {
      $dep_list[] = "drupal/core-dev";
      $dep_list[] = "drupal/core-dev-pinned";
      $this->shell->passthru("{$composer_command} -n remove --dev drupal/core-dev");
      $this->shell->passthru("{$composer_command} -n require --dev drupal/core-dev:${versionConstraint} drupal/core-dev-pinned:${versionConstraint}");
      $this->shell->passthru("{$composer_command} -n remove --dev --no-update drupal/core-dev-pinned");
      $this->shell->passthru("{$composer_command} -n update drupal/core-dev-pinned");
    }

    // Revert any changes to composer.json made by the 'require'.
    $this->shell->passthru("git -C {$directory} -c advice.detachedHead=false checkout -- composer.json");

    // Updates the content hash of the reverted composer.json in the lockfile
    $this->shell->passthru("{$composer_command} -n update --lock --ignore-platform-reqs");

    // Make sure that the paths in composer.lock point to github, and not our local path repos.
    $this->fixLockFile($directory, $dep_list);

    $this->shell->passthru("git -C {$directory} add composer.lock");

    $original_committer_date = $this->shell->exec("git -C {$directory} log -1 --pretty=%cd");
    $this->shell->passthru("GIT_COMMITTER_DATE=\"{$original_committer_date[0]}\" git -C {$directory} commit --amend -C HEAD");

    // The --force is here because if we're changing the composer.json with
    // amended commits, the push will fail
    if ($reftype == 'tag') {
      $this->shell->passthru("git -C {$directory} tag -f {$ref}");
    }

    $this->shell->passthru("git -C {$directory} push --force vault $ref");
    $this->shell->passthru("git -C {$directory} clean -ffd .");

    return TRUE;
  }

  protected function fixLockedPackages($package_list, $dep_names) {
    $package_list = array_map(
      static function ($package) use ($dep_names) {
        if (in_array($package['name'], $dep_names)) {
          $package['source']['type'] = "git";
          $package['source']['url'] = "https://github.com/{$package['name']}.git";
          $package['source']['reference'] = $package['dist']['reference'];
          $package['dist']['type'] = "zip";
          $package['dist']['url'] = "https://api.github.com/repos/{$package['name']}/zipball/{$package['dist']['reference']}";
          $package['dist']['shasum'] = "";
          unset($package['transport-options']);
        }
        return $package;
      },
      $package_list
    );
    return $package_list;
  }

  protected function stabilityFromRef($ref): string {
    if (preg_match('#^[0-9.]*$#', $ref)) {
      return 'stable';
    }
    if (stripos($ref, '-rc') !== FALSE) {
      return 'rc';
    }
    if (stripos($ref, '-beta') !== FALSE) {
      return 'beta';
    }
    if (stripos($ref, '-alpha') !== FALSE) {
      return 'alpha';
    }
    return 'dev';
  }

  protected function versionConstraintFromRef($ref) {
    if (preg_match('#\.x$#', $ref)) {
      return "$ref-dev";
    }
    return $ref;
  }

  protected function configureProjectComposerJson($directory, $stability) {
    if ($stability !== 'dev') {
      $composer_json = json_decode(file_get_contents("$directory/composer.json"), TRUE);
      unset($composer_json['require-dev']);
      file_put_contents("$directory/composer.json", json_encode($composer_json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
      $this->shell->passthru("git -C {$directory} add composer.json");

      $original_committer_date = $this->shell->exec("git -C {$directory} log -1 --pretty=%cd");
      $this->shell->passthru("GIT_COMMITTER_DATE=\"{$original_committer_date[0]}\" git -C {$directory} commit --amend -C HEAD");
    }
  }

  protected function getProjectsToConstrain($directory) {
    $composer_json = json_decode(file_get_contents("$directory/composer.json"), TRUE);

    return array_filter(
      array_keys($composer_json['require']),
      static function ($dep) {
        return strpos($dep, 'drupal/') !== FALSE;
      }
    );
  }

  /**
   * @param $directory
   * @param array $dep_list
   */
  protected function fixLockFile($directory, array $dep_list) {
    // Add core to the list that we fix.
    $dep_list[] = 'drupal/core';
    $composer_lock = json_decode(file_get_contents("$directory/composer.lock"), TRUE);
    $composer_lock['packages'] = $this->fixLockedPackages($composer_lock['packages'], $dep_list);
    $composer_lock['packages-dev'] = $this->fixLockedPackages($composer_lock['packages-dev'], $dep_list);
    file_put_contents("$directory/composer.lock", json_encode($composer_lock, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
  }

  /**
   * Gets the minimum php version constraint from core/composer.json.
   *
   * This method assumes that the php version constraint uses `>=`. This is
   * pretty much all anyone uses for php, so we do not bother with other
   * variations. This method will not work if something else is used fro some
   * reason.
   *
   * @return bool|string
   *   PHP minimum version, or FALSE if it is not available or unknown.
   */
  protected function findMinimumPhp() {
    if (!file_exists("{$this->getProjectSourceDir()}/core/composer.json")) {
      return FALSE;
    }
    $json = json_decode(file_get_contents("{$this->getProjectSourceDir()}/core/composer.json"), TRUE);
    if (!isset($json['require']['php'])) {
      return FALSE;
    }
    $php_constraint = $json['require']['php'];
    $minimum_php = str_replace('>=', '', $php_constraint);
    $minimum_php = str_replace('^', '', $minimum_php);
    if (preg_match('#[^0-9.]#', $minimum_php)) {
      return FALSE;
    }

    return $minimum_php;
  }

}
